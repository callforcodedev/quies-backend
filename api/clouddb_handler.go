package api

import (
	"fmt"
	"os"

	"github.com/IBM-Bluemix/go-cloudant"
	rep "github.com/cristianchaparroa/quies/repositories"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

// SyncCloudDB syn the local database with cloud
func SyncCloudDB(db *gorm.DB) gin.HandlerFunc {

	fn := func(c *gin.Context) {

		user := os.Getenv("USER_CLOUDANT")
		pass := os.Getenv("PASSWORD_CLOUDANT")
		//	url := os.Getenv("URL_CLOUDANT")

		client, err := cloudant.NewClient(user, pass)

		if err != nil {
			fmt.Println(err)
		}

		fmt.Println(client)
		r := rep.NewPersonRepository(db)

		people := make([]interface{}, 0)

		limit := 2
		pg := r.FindAll(0, limit)

		people = append(people, pg.Records)

		fmt.Println(pg.TotalPage)

		for i := 1; i < pg.TotalPage; i++ {
			pg := r.FindAll(pg.NextPage, limit)
			people = append(people, pg.Records)
		}

		documentDB, err := client.EnsureDB("onquies")

		if err != nil {
			c.String(500, err.Error())
			return
		}

		for _, p := range people {
			fmt.Println(p)
			documentDB.CreateDocument(p)
		}

		c.String(200, "")

	}

	return gin.HandlerFunc(fn)
}
