package api

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/cristianchaparroa/quies/models"
	rep "github.com/cristianchaparroa/quies/repositories"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

// CreatePerson creates a new register with data about an specific person
func CreatePerson(db *gorm.DB) gin.HandlerFunc {

	fn := func(c *gin.Context) {
		fmt.Println("--> CreatePerson")
		var p models.Person

		if err := c.BindJSON(&p); err != nil {
			c.String(http.StatusMethodNotAllowed, fmt.Sprintf("Ivalid input:%s", err.Error()))
			fmt.Println("<-- CreatePerson")
			return
		}

		/*
			r := rep.NewPersonRepository(db)

			e := r.FindById(p.ID)

			if e != nil && e.ID != "" {
				c.String(400, fmt.Sprintf("Person already exist:%v", e))
				fmt.Println("<-- CreatePerson")
				return
			}
		*/

		now := time.Now()
		p.CreatedAt = now

		p.Names = strings.ToUpper(p.Names)

		err := db.Create(&p)
		fmt.Println(err)

		fmt.Println("<-- CreatePerson")
		c.JSON(201, p)
	}

	return gin.HandlerFunc(fn)
}

// GetPerson retrieves the information about the Person
func GetPerson(db *gorm.DB) gin.HandlerFunc {

	fn := func(c *gin.Context) {

		fmt.Println("--> GetPerson")
		id := c.Param("id")

		fmt.Println()
		r := rep.NewPersonRepository(db)

		p := r.FindByIdOrNames(id)

		if p == nil || len(p) == 0 {
			c.String(http.StatusNotFound, "Person not found")
			return
		}

		fmt.Println(p)
		fmt.Println("<-- GetPerson")
		c.JSON(200, p)
	}

	return gin.HandlerFunc(fn)
}
