package api

import (
	"fmt"
	"os"

	"github.com/cristianchaparroa/quies/initializer"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"

	_ "github.com/lib/pq"
)

// Server struct creates the api server
type Server struct {
	Engine *gin.Engine
	DB     *gorm.DB
}

// NewServer generates a pointer to Server
func NewServer() *Server {
	e := gin.Default()
	return &Server{Engine: e}
}

// Setup is in charge to configure al l related with services
func (s *Server) Setup() {
	s.setupDB()
	s.setupRouter()
}

// setupDB is in charge to setup the database
func (s *Server) setupDB() {
	user := os.Getenv("USER_DB")
	pass := os.Getenv("PASSWORD_DB")
	dbName := os.Getenv("NAME_DB")
	host := os.Getenv("HOST_DB")

	datasource := fmt.Sprintf("postgresql://%s:%s@%s/%s?sslmode=disable", user, pass, host, dbName)

	db, err := gorm.Open("postgres", datasource)
	//db.LogMode(true)

	if err != nil {
		panic(err)
	}

	s.DB = db

	im := initializer.NewInitialzerManager()
	im.Run(s.DB)
}

func (s *Server) setupRouter() {
	s.Engine.Use(CORS())
	s.Engine.POST("/v1/api/person", CreatePerson(s.DB))
	s.Engine.GET("/v1/api/person/:id", GetPerson(s.DB))
	s.Engine.POST("/v1/api/cloud/db/sync", SyncCloudDB(s.DB))
}

// Run  execut the api.
func (s *Server) Run() {
	s.Engine.Run(":80")
}

// CORS adds a middleware for request througth gin-gonic
func CORS() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, DELETE")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
