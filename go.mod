module github.com/cristianchaparroa/quies

go 1.12

require (
	cloud.google.com/go v0.41.0 // indirect
	github.com/IBM-Bluemix/go-cloudant v0.0.0-20161013050022-0ded5c7f524e
	github.com/biezhi/gorm-paginator/pagination v0.0.0-20190124091837-7a5c8ed20334
	github.com/cloudant-labs/go-cloudant v0.0.0-20180620160115-81a70ee15c97
	github.com/gin-contrib/sse v0.1.0 // indirect
	github.com/gin-gonic/gin v1.4.0
	github.com/jinzhu/gorm v1.9.10
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.1.1
	github.com/mattn/go-isatty v0.0.8 // indirect
	github.com/moul/http2curl v1.0.0 // indirect
	github.com/parnurzeal/gorequest v0.2.15 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/timjacobi/go-couchdb v0.0.0-20160817113035-5f9d2a1a29e5 // indirect
	github.com/ugorji/go v1.1.7 // indirect
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4 // indirect
	golang.org/x/exp v0.0.0-20190627132806-fd42eb6b336f // indirect
	golang.org/x/image v0.0.0-20190703141733-d6a02ce849c9 // indirect
	golang.org/x/mobile v0.0.0-20190607214518-6fa95d984e88 // indirect
	golang.org/x/net v0.0.0-20190628185345-da137c7871d7 // indirect
	golang.org/x/sys v0.0.0-20190626221950-04f50cda93cb // indirect
	golang.org/x/tools v0.0.0-20190706070813-72ffa07ba3db // indirect
	google.golang.org/genproto v0.0.0-20190701230453-710ae3a149df // indirect
	google.golang.org/grpc v1.22.0 // indirect
)
