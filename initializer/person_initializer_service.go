package initializer

import (
	"github.com/cristianchaparroa/quies/models"
	"github.com/jinzhu/gorm"
)

// PersonInitializerService is in charge to create a Person table
type PersonInitializerService struct {
	DB *gorm.DB
}

// NewPersonInitialzerService generates a pointer to PersonInitializerService
func NewPersonInitialzerService(db *gorm.DB) *PersonInitializerService {
	return &PersonInitializerService{DB: db}
}

// Execute is charge to create the Person table
func (s *PersonInitializerService) Execute() error {

	if !s.shouldRun() {
		return nil
	}

	s.CreateTables()

	return nil
}

// shouldRun determintes if the initializer was executed
func (s *PersonInitializerService) shouldRun() bool {

	if s.DB.HasTable(&models.Person{}) {
		return false
	}
	return true

}

// CreateTables create a table
func (s *PersonInitializerService) CreateTables() {
	s.DB.CreateTable(&models.Person{})
}
