package main

import (
	"github.com/cristianchaparroa/quies/api"
	"github.com/joho/godotenv"
)

func main() {

	err := godotenv.Load()

	if err != nil {
		panic(err)
	}

	s := api.NewServer()
	s.Setup()
	s.Run()
}
