package models

import (
	"fmt"
	"time"

	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
)

// Person continas the data related with a person in a disaster
type Person struct {
	UUID  uuid.UUID `gorm:"type:uuid;primary_key;" json:"uuid"`
	ID    string    `json:"id"`
	Names string    `json:"names"`
	Age   string    `json:"age"`
	Genre string    `json:"genre"`

	// RH factor blood
	RH string `json:"rh"`
	// The date and time of the creation
	CreatedAt time.Time `json:"createdAt"  time_format:"2006-01-02 15:

	:05"`

	Description string `json:"description"`

	Status string `json:"status"`
}

// BeforeCreate will set a UUID rather than numeric ID.
func (s *Person) BeforeCreate(scope *gorm.Scope) error {
	uuid := uuid.NewV4()
	return scope.SetColumn("uuid", uuid.String())
}

func (s *Person) String() string {
	return fmt.Sprintf("UUID:%s, id:%s, Name:%s", s.UUID.String(), s.ID, s.Names)
}
