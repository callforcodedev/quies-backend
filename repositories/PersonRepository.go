package repositories

import (
	"strings"

	pagination "github.com/biezhi/gorm-paginator/pagination"
	"github.com/cristianchaparroa/quies/models"
	"github.com/jinzhu/gorm"
)

// PersonRepository is in charge to interact with person modes
type PersonRepository struct {
	DB *gorm.DB
}

// NewPersonRepository generates a pointer to PersonRepository
func NewPersonRepository(db *gorm.DB) *PersonRepository {
	return &PersonRepository{DB: db}
}

// FindById retrieves a specific person acording with id
func (r *PersonRepository) FindById(id string) []models.Person {
	var ps []models.Person
	r.DB.Table("people").Where("id = ?", id).Find(&ps)
	return ps
}

// FindByNames find by names
func (r *PersonRepository) FindByNames(names string) []models.Person {
	var ps []models.Person
	r.DB.Table("people").Where("name LIKE ?", "%"+names+"%").Find(&ps)
	return ps
}

// FindByIdOrNames ...
func (r *PersonRepository) FindByIdOrNames(q string) []models.Person {
	var ps []models.Person

	q = strings.ToUpper(q)
	r.DB.Table("people").Where("names LIKE ? OR id like ?", "%"+q+"%", q).Find(&ps)
	return ps
}

// FindAll find people
func (r *PersonRepository) FindAll(page, limit int) *pagination.Paginator {

	var people []models.Person

	paginator := pagination.Paging(&pagination.Param{
		DB:      r.DB,
		Page:    page,
		Limit:   limit,
		OrderBy: []string{"created_at asc"},
		ShowSQL: true,
	}, &people)

	return paginator
}
